#! /usr/bin/make -f

# Comment this out to turn off verbose mode.
#export DH_VERBOSE=1

# Automake 1.14 warns about not using subdir-objects,
# but using this option breaks the build, see:
#    http://debbugs.gnu.org/cgi/bugreport.cgi?bug=16375
#    http://debbugs.gnu.org/cgi/bugreport.cgi?bug=13928
# The following variables can be used to override the automake/aclocal versions.
#export AUTOMAKE = automake
#export ACLOCAL = aclocal
include /usr/share/dpkg/pkg-info.mk
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# This is needed to work around arch-specific quirks in the test suite.
DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

# Get the hardening build flags and enable all compiler warnings:
CFLAGS   := $(shell dpkg-buildflags --get CFLAGS) -Wall -D_FILE_OFFSET_BITS=64
CXXFLAGS := $(shell dpkg-buildflags --get CXXFLAGS) -Wall -D_FILE_OFFSET_BITS=64
CPPFLAGS := $(shell dpkg-buildflags --get CPPFLAGS)
LDFLAGS  := $(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed

# LLVM is only used on these archs, so no point in enabling it elsewhere
# To add new llvm/jit archs add it here and to the arch list for llvm-dev in
# debian/control.
# PowerPC is disabled until atleast llvm-3.8. As thing stand now MCJIT is
# broken in llvm-3.6.
# Currently LLVM is disabled because there is no support for llvm > 3.8
#ifeq (,$(filter $(DEB_HOST_ARCH), i386 amd64 kfreebsd-amd64 kfreebsd-i386))
  export enable_llvm=no
#else
#    SYSTEM_LLVM = -with-system-llvm=/usr/bin/llvm-config --with-llvm-linking=dynamic
#endif

# Set the configure options:
#	* add the build flags
#	* set various paths
#	* disable test for clamav user/group (--disable-clamav)
#	* disable building of the non-free libclamunrar (--disable-unrar)
#	* build clamav-milter (--enable-milter)
#	* enable workaround for broken DNS servers (--enable-dns-fix)
#	* assume the C compiler uses GNU ld (--with-gnu-ld)
#	* use system libraries instead of the embedded copies (--with-system-tommath, --without-included-ltdl, -with-system-llvm=/usr/bin/llvm-config)
CONFIG := CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS)" CXXFLAGS="$(CXXFLAGS)" LDFLAGS="$(LDFLAGS)" \
	--with-dbdir=/var/lib/clamav --sysconfdir=/etc/clamav \
	--disable-clamav --disable-unrar --enable-milter --enable-dns-fix \
	--with-libjson --with-system-libmspack --with-libcurl=/usr \
	--with-gnu-ld $(SYSTEM_LLVM) \
	--with-systemdsystemunitdir=/lib/systemd/system

DEBUG_OPTS=

# Enable debug code, if nostrip was given.
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
else
	DEBUG_OPTS += --enable-debug
endif

%:
	dh $@


override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info

override_dh_auto_configure:
	$(info DEB_BUILD_OPTIONS = $(DEB_BUILD_OPTIONS))
	$(info CFLAGS = $(CFLAGS))
	$(info CXXFLAGS = $(CXXFLAGS))
	$(info CPPFLAGS = $(CPPFLAGS))
	$(info LDFLAGS = $(LDFLAGS))
	# Check for unknown options in the configuration files.
	egrep '^#[[:alpha:]]' etc/clamd.conf.sample | sed -e 's/^#//' | awk '{print $$1}' | while read opt; do \
	  if ! grep -q "$${opt}" debian/clamav-daemon.postinst.in ; then \
	    echo "Unhandled option(clamd.conf): $${opt}"; \
	    touch debian/exit; \
	  fi;\
	done; \
	egrep '^#[[:alpha:]]' etc/freshclam.conf.sample | sed -e 's/^#//' | awk '{print $$1}' | while read opt; do \
	  if ! grep -q "$${opt}" debian/clamav-freshclam.postinst.in ; then \
	    echo "Unhandled option (freshclam.conf): $${opt}"; \
	    touch debian/exit; \
	  fi;\
	done; \
	egrep '^#[[:alpha:]]' etc/clamav-milter.conf.sample | sed -e 's/^#//' | awk '{print $$1}' | while read opt; do \
	  if ! grep -q "$${opt}" debian/clamav-milter.postinst.in ; then \
	    echo "Unhandled option (clamav-milter.conf): $${opt}"; \
	    touch debian/exit; \
	  fi;\
	done; \
	# Check for library features which may have been upgraded.
	if ! grep -q "CL_FLEVEL 123" libclamav/others.h ; then \
		echo "cl_retflevel needs boosting in symbol file"; \
		touch debian/exit; \
	fi;
	sed "s/ \(.*\)@CLAMAV_PRIVATE .*/ \1@CLAMAV_PRIVATE $(shell echo $(DEB_VERSION) | cut -d+ -f1)/" \
		< debian/libclamav9.symbols > debian/libclamav9.symbols.latest
	if ! cmp debian/libclamav9.symbols debian/libclamav9.symbols.latest; then \
		echo "Need to update CLAMAV_PRIVATE symbols"; \
		touch debian/exit; \
	fi;

	[ ! -f debian/exit ] || (rm debian/exit && exit 1)
	rm debian/libclamav9.symbols.latest

	# Configure using the specified flags.
	dh_auto_configure -- $(CONFIG) $(DEBUG_OPTS)

override_dh_auto_clean:
	# Update .po files for debconf.
	debconf-updatepo
	dh_auto_clean

clamav_build-indep:
	# Add common functions in config and postinst files.
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-base.postinst.in > debian/clamav-base.postinst

clamav_build-arch:
	# Add common functions in config and postinst files.
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-milter.init.in > debian/clamav-milter.init
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-milter.config.in > debian/clamav-milter.config
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-milter.postinst.in > debian/clamav-milter.postinst
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-daemon.config.in > debian/clamav-daemon.config
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-daemon.postinst.in > debian/clamav-daemon.postinst
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-daemon.init.in > debian/clamav-daemon.init
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-freshclam.init.in > debian/clamav-freshclam.init
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-freshclam.config.in > debian/clamav-freshclam.config
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-freshclam.postinst.in > debian/clamav-freshclam.postinst
	perl -pe 's~#COMMON-FUNCTIONS#~qx{cat debian/common_functions}~eg' < debian/clamav-freshclam.prerm.in > debian/clamav-freshclam.prerm

override_dh_auto_build-indep: clamav_build-indep
	# Build architecture-independent files.
	dh_auto_build -i -- V=1

override_dh_auto_build-arch: clamav_build-arch
	# Build architecture-dependent files.
	dh_auto_build -a -- V=1

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifneq (,$(filter ia64 sparc, $(DEB_HOST_ARCH)))
	T=900 $(MAKE) check EF_ALIGNMENT=8 V=1 VERBOSE=1
else
	T=900 $(MAKE) check V=1 VERBOSE=1
endif
endif

override_dh_install:
	dh_install
	dh_apparmor -pclamav-freshclam --profile-name=usr.bin.freshclam
	dh_apparmor -pclamav-daemon --profile-name=usr.sbin.clamd

override_dh_installinit:
	dh_installinit -pclamav-daemon
	# Don't change the postinst/postrm scripts for clamav-freshclam, as they need non-standard code.
	dh_installinit -pclamav-freshclam --noscripts
	dh_installinit -pclamav-milter

override_dh_installdocs-indep:
	dh_installdocs
	# Make the .rar test files which are no longer created
	cat test/.split/split.clam-v2.raraa test/.split/split.clam-v2.rarab >debian/clamav-testfiles/usr/share/clamav-testfiles/clam-v2.rar
	cat test/.split/split.clam-v3.raraa test/.split/split.clam-v3.rarab >debian/clamav-testfiles/usr/share/clamav-testfiles/clam-v3.rar

# dh_strip_nondeterminism breaks .zip files. See #817943
override_dh_strip_nondeterminism:
	dh_strip_nondeterminism -Xclam.bz2.zip -Xclam.d64.zip -Xclam.impl.zip -Xclam.zip

# Don't compress the example configuration files and the documentation PDFs.
override_dh_compress:
	dh_compress -Xexamples
